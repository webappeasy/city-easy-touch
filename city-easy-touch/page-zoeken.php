<?php include_once 'assets/header.php';
?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
    <h4 class="text-muted" id="explore-filters-1 text-muted"><?php echo the_title(); ?></h4></a>
    <hr>
    <div>
        <h5 id="PagResultsMenu"><a id="SearchPageResults" class="text-muted">Pagina Resultaten</a> <span id="resultsRightPage" class="resultsRight">0</span></h5>
        <h5 id="EveResultsMenu"><a id="SearchEventResults" class="text-muted">Evenementen Resultaten</a> <span id="resultsRightEvent" class="resultsRight">0</span></h5>
        <h5 id="OntResultsMenu"><a id="SearchOntdekResults" class="text-muted">Ontdekken Resultaten</a> <span id="resultsRightOntdek" class="resultsRight">0</span></h5>
        <h5 id="ZidResultsMenu"><a id="SearchZieDoeResults" class="text-muted">Zien & doen Resultaten</a> <span id="resultsRightZieDoe" class="resultsRight">0</span></h5>
        <h5 id="NewResultsMenu"><a id="SearchNieuwsResults" class="text-muted">Nieuws Resultaten</a> <span id="resultsRightNieuws" class="resultsRight">0</span></h5>
    </div>
    <div class="scrollDiv">
        <button id="topBut" class="btn btn-primary d-block disabled"><i class="fa fa-arrow-up"></i></button>
        <button id="downBut" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div>
        <div id="algemeen" class="bg-light rounded shadow p-3 mb-5">
                <h5 class="text-muted" id="pageResults">Pagina resultaten</h5>
                <?php

                if (isset($s) && !empty($s)) {
                    echo "<input id='searchVal' type='hidden' value='".$s."'>";
                }

                $args = array(
                    'post_type'      => 'page',
                    'posts_per_page' => -1,
                    'order'          => 'ASC',
                    'orderby'        => 'menu_order',
                    's'              => $s
                );
                $parent = new WP_Query( $args );
                
                if ( $parent->have_posts() ) : ?>
                    <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                        <div id="parent-<?php the_ID(); ?>" class="parent-page">
                            <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h5>
                        </div>
                    <?php endwhile; ?>
                <?php endif; wp_reset_postdata(); ?>

                <hr class="line-break">
                <h5 class="text-muted" id="eventResults">Evenementen resultaten</h5>

                <div class="row animated fadeIn">
                    <div class="container-fluid">
                        <div class="loader m-auto"></div>
                        <div id="javascript-events"></div>
                        <script src="<?php echo get_template_directory_uri() . '/js/api-events.js';?>"></script>
                    </div>
                </div>

                <hr class="line-break">
                <h5 class="text-muted" id="ontdekResults">Ontdekken resultaten</h5>

                <div class="row animated fadeIn">
                    <div class="container-fluid">
                        <div class="loader m-auto"></div>
                        <div id="javascript-explore"></div>
                        <script src="<?php echo get_template_directory_uri() . '/js/api-explore.js';?>"></script>
                    </div>
                </div>

                <hr class="line-break">
                <h5 class="text-muted" id="ZieDoResults">Zien & doen resultaten</h5>

                <div class="row animated fadeIn">
                    <div class="container-fluid">
                        <div class="loader m-auto"></div>
                        <div id="javascript-activities"></div>
                        <script src="<?php echo get_template_directory_uri() . '/js/api-activities.js';?>"></script>
                    </div>
                </div>

                <hr class="line-break">
                <h5 class="text-muted" id="nieuwResults">Nieuws resultaten</h5>

                <div class="row animated fadeIn">
                    <div class="container-fluid">
                        <div class="loader m-auto"></div>
                        <div id="javascript-news"></div>
                        <script src="<?php echo get_template_directory_uri() . '/js/api-news.js';?>"></script>
                    </div>
                </div>
        </div>
    </div>
</div>

<!-- Right sidebar -->
<?php 
include_once 'assets/sidebar.php';
// Footer
include_once 'assets/footer.php'; 
?>