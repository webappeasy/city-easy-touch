<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

$description = (is_front_page()) ? get_bloginfo('description') : get_the_title();
if (is_search()) $description = __('Zoekopdracht', 'city-easy-touch');
if (is_404()) $description = __('Pagina niet gevonden', 'city-easy-touch');

// Grab the url for the full size featured image
$featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');

switch ($my_current_lang) {
    case 'de':
        $eventText = "Veranstaltungen";
        $discoverText = "Entdecken";
        $seedoText = "Sehen & tun";
        $newsText = "Nachrichten";
        $generalLink = "/de/allgemeines";
        $generalText = "Allgemeines";
    break;
    case 'en':
        $eventText = "Events";
        $discoverText = "To discover";
        $seedoText = "See & do";
        $newsText = "News";
        $generalLink = "/en/general";
        $generalText = "General";
    break;
    default:
        $eventText = "Evenementen";
        $discoverText = "Ontdekken";
        $seedoText = "Zien & doen";
        $newsText = "Nieuws";
        $generalLink = "algemeen";
        $generalText = "Algemeen";
}
?>
<!-- Web application created by Web & App Easy | https://www.webandappeasy.com 
__          __  _                       _                          ______ 
\ \        / / | |                     | |     /\                 |  ____|   
 \ \  /\  / /__| |__     __ _ _ __   __| |    /  \   _ __  _ __   | |__   __ _ ___ _   _ 
  \ \/  \/ / _ \ '_ \   / _` | '_ \ / _` |   / /\ \ | '_ \| '_ \  |  __| / _` / __| | | |
   \  /\  /  __/ |_) | | (_| | | | | (_| |  / ____ \| |_) | |_) | | |___| (_| \__ \ |_| |
    \/  \/ \___|_.__/   \__,_|_| |_|\__,_| /_/    \_\ .__/| .__/  |______\__,_|___/\__, |
                                                    | |   | |                      __/ /
                                                    |_|   |_|                     |___/ 
-->
<!DOCTYPE html>
<html class="h-100" <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset');?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
    <title><?php echo get_bloginfo('name');?> | <?php echo __($description, 'city-easy-touch');?></title>
    <?php wp_head();?>
</head>
<body>
<header class="sticky-top">
        <div class="container-fluid p-0 overflow-hidden position-relative banner" style="background-color: #343a40;">
            <div class="col-12 p-0 banner-image bg-light" style="background-image: url('<?php echo esc_url($featured_img_url); ?>');height: 250px;background-position: center;background-size: cover;"></div>
            <div class="banner-heading container d-flex align-items-center h-100" style="position: absolute;top: 0;left: 0;right: 0;text-shadow: 0 0 25px rgba(0,0,0,.75);">
                <div class="row">
                    <div class="col text-white animated pulse delay-2s">
                        <h1 class="display-3 mb-0 text-break"><?php echo get_the_title(); ?></h1>
                        <p class="mb-0">
                            <?php 
                                switch ($my_current_lang) {
                                    case 'de':
                                        echo "Sonne, Meer, Strand und vieles mehr!";
                                        $startButton = "Startseite";
                                        $searchInp = "Suche...";
                                        $searchText = "Suche";
                                        $searchlink = "/de/suchen/";
                                    break;
                                    case 'en':
                                        echo "Sun, sea, beach and much more!";
                                        $startButton = "Homepage";
                                        $searchInp = "Search...";
                                        $searchText = "Search";
                                        $searchlink = "/en/search/";
                                    break;
                                    default:
                                        echo "Zon, zee, strand en veel meer!";
                                        $startButton = "Startpagina";
                                        $searchInp = "Zoeken...";
                                        $searchText = "Zoeken";
                                        $searchlink = "/zoeken/";
                                }

                                if (isset($_GET["search"])&&!empty($_GET["search"])) {
                                    $s = $_GET["search"];
                                    $searchInp=$s."...";
                                }
                            ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-dark container-fluid shadow" style="min-height: 101px;background-color: #252a2e!important;">
            <div class="row">
                <div class="col-xl-4 h-100"><img class="mt-2" src="<?php echo get_theme_file_uri() . '/img/logo_raz.svg'; ?>" style="height: 84px;"></div>
                <div class="col-xl-4 text-center"><a class="btn btn-secondary btn-lg mt-4" role="button" href="<?php echo home_url(); ?>"><?php _e($startButton, 'city-easy-touch'); ?></a></div>
                <div class="col-xl-4">
                    <form class="form-inline mt-4 d-block text-right" action="<?php echo $searchlink; ?>"><input class="form-control form-control-lg use-keyboard-input mr-sm-2" id="search" name="search" type="search" placeholder="<?php _e($searchInp, 'city-easy-touch'); ?>">
                    <button class="btn btn-primary btn-lg mr-2" id="searchBut" type="button"><?php _e($searchText, 'city-easy-touch'); ?></button>
                    <button class="btn btn-danger btn-lg" data-aos="zoom-in-down" data-aos-duration="1000" data-aos-delay="600" type="button" data-toggle="modal" data-target="#touchExplanation"><i class="fa fa-question-circle"></i></button>
                    <?php do_action('wpml_add_language_selector'); ?></form>
                </div>
            </div>
        </div>
    </header>
    <main class="container-fluid flex-shrink-0" role="main">
        <div class="row">