<div class="bg-light col-xl-2 text-right pt-3 shadow" data-aos="fade-left" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__right">
    <h4 class="text-muted">Newsletter</h4>
    <p class="text-muted">Bleib informiert</p>
    <form method="post" action="<?php echo get_theme_file_uri();?>/mailchimp-subscribe-api.php">
        <div class="form-group">
            <input class="form-control mb-2 use-keyboard-input" type="text" placeholder="Ihr Vorname" name="fname">
            <input class="form-control mb-2 use-keyboard-input" type="text" placeholder="Dein Familienname" name="lname">
            <input class="form-control mb-2 use-keyboard-input" type="email" placeholder="Deine E-Mail-Adresse" name="email">
            <input class="form-control btn btn-primary mb-2 text-left" type="submit" value="Registrieren Sie sich sofort" name="submit">
        </div>
    </form>   
    <hr>
    <h4 class="text-muted">Renesse app</h4>
    <p class="text-muted">In den App Stores herunterladen</p><img class="p-1" src="<?php echo get_theme_file_uri();?>/img/renesse-app-1.png" style="width: 100%;">
</div>
