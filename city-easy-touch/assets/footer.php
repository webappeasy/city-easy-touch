<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;
?>
    </div>
</main>
<!-- Modal Touchscreen Explanation -->
<footer id="touchExplanation" class="modal fade">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Touchscreen Renesse uitleg</h4><button class="btn btn-secondary" type="button" data-dismiss="modal">×</button></div>
                <div class="modal-body">
                    <h4>Wat is Touchscreen Renesse</h4>
                    <p> Touchscreen Renesse is een informatiezuil waar u aankomende evenementen en andere leuke activiteiten in en rond Renesse kunt vinden. Doormiddel van gefilterde zoekopdrachten kunt u leuke activiteiten in de buurt vinden en direct meenemen in uw smartphone middels het scannen van een QR Code.</p>
                    <h4>Filteren</h4>
                    <p>Op bijna iedere pagina kunt u links in het scherm filteren. Hierdoor kunt u bijvoorbeeld snel evenementen vinden die voldoen aan uw zoekopdracht. Aan het begin staan alle opties aan. Door er op te tikken kunt u de zoek opdrachten aanpassen en zal gefilterd worden naar de ingevoerde data. Om de weg gefilterde data terug te krijgen drukt u nog eens op dezelfde knop, zodat er weer Aan staat.</p>
                    <h4>Nieuwsbrief</h4>
                    <p>Wilt u op de hoogte blijven van de gebeurtenissen in en om Renesse? Schrijf u dan rechts in het scherm in voor de Nieuwsbrief. U blijft dan op de hoogte van de aankomende evenementen en activiteiten in en om Renesse. Het enige wat u hoeft te doen is op de invul regels te klikken. Er verschijnt automatisch een toetsenbord in het scherm waarmee u de gegevens die nodig zijn kunt invullen. Vul wel alles in! Zijn er velden niet ingevuld? Dan is de aanmelding niet geldig.</p>
                    <h4>Scrollen</h4>
                    <p>Wilt u op de pagina scrollen maar lukt dit niet makkelijk? Geen zorgen links op de pagina ziet u 2 knoppen onder elkaar. De één bevat een pijl omhoog en de ander een pijl omlaag. Hiermee kunt u ook scrollen! Het enige wat u hoeft te doen is erop te klikken.</p>
                </div>
            <div class="modal-footer"><button class="btn btn-danger" type="button" data-dismiss="modal">Sluiten</button></div>
        </div>
    </div>
</footer>


<?php if (strpos($_SERVER['REQUEST_URI'], "evenementen") !== false || strpos($_SERVER['REQUEST_URI'], "zoeken") !== false){ ?>
    <!-- Modal Events, only visible when on /events -->
    <footer id="eventInformation" class="modal fade">
    <div class="scrollModel">
        <button id="topModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-up"></i></button>
        <button id="downModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div id="modal-header" class="modal-header" style="height:300px;">
                    <button class="btn btn-dark modal-close-btn" type="button" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body pt-0 pb-0">
                    <div class="row">
                        <div class="col-8 pt-3">
                            <h2 class="modal-event-title"></h2>
                            <p class="modal-event-content pt-3"></p>
                        </div>
                        <div class="col-4 pt-3 bg-light text-center">
                            <h5>Evenement gegevens</h5>
                            <p class="modal-event-date"></p>
                            <h5>Contact gegevens</h5>
                            <p class="modal-event-phone mb-0"></p>
                            <p class="modal-event-email text-break"></p>
                            <h5>Gegevens meenemen</h5>
                            <p>Scan de QR-code:</p>
                            <div id="google-apis-qr-code"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php if (strpos($_SERVER['REQUEST_URI'], "ontdekken") !== false || strpos($_SERVER['REQUEST_URI'], "zoeken") !== false){ ?>
    <!-- Modal Explore, only visible when on /explore -->
    <footer id="exploreInformation" class="modal fade">
    <div class="scrollModel">
        <button id="topModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-up"></i></button>
        <button id="downModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div id="modal-header" class="modal-header" style="height:300px;">
                    <button class="btn btn-dark modal-close-btn" type="button" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body pt-0 pb-0">
                    <div class="row">
                        <div class="col-8 pt-3">
                            <h2 class="modal-explore-title"></h2>
                            <p class="modal-explore-content pt-3"></p>
                        </div>
                        <div class="col-4 pt-3 bg-light text-center">
                            <h5>Contact gegevens</h5>
                            <p class="modal-explore-phone mb-0"></p>
                            <p class="modal-explore-email text-break"></p>
                            <h5>Gegevens meenemen</h5>
                            <p>Scan de QR-code:</p>
                            <div id="google-apis-qr-code"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php if (strpos($_SERVER['REQUEST_URI'], "activiteiten") !== false || strpos($_SERVER['REQUEST_URI'], "zoeken") !== false){ ?>
    <!-- Modal activity, only visible when on /activities -->
    <footer id="activityInformation" class="modal fade">
    <div class="scrollModel">
        <button id="topModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-up"></i></button>
        <button id="downModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div id="modal-header" class="modal-header" style="height:300px;">
                    <button class="btn btn-dark modal-close-btn" type="button" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body pt-0 pb-0">
                    <div class="row">
                        <div class="col-8 pt-3">
                            <h2 class="modal-activity-title"></h2>
                            <p class="modal-activity-content pt-3"></p>
                        </div>
                        <div class="col-4 pt-3 bg-light text-center">
                            <h5>Gegevens meenemen</h5>
                            <p>Scan de QR-code:</p>
                            <div id="google-apis-qr-code"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php if (strpos($_SERVER['REQUEST_URI'], "nieuws") !== false || strpos($_SERVER['REQUEST_URI'], "zoeken") !== false){ ?>
    <!-- Modal newsitem, only visible when on /news -->
    <footer id="newsInformation" class="modal fade">
    <div class="scrollModel">
        <button id="topModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-up"></i></button>
        <button id="downModelButton" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content">
                <div id="modal-header" class="modal-header" style="height:300px;">
                    <button class="btn btn-dark modal-close-btn" type="button" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body pt-0 pb-0">
                    <div class="row">
                        <div class="col-8 pt-3">
                            <h2 class="modal-news-title"></h2>
                            <p class="modal-news-content pt-3"></p>
                        </div>
                        <div class="col-4 pt-3 bg-light text-center">
                            <h5>Publicatiedatum</h5>
                            <p class="modal-news-date"></p>
                            <h5>Gegevens meenemen</h5>
                            <p>Scan de QR-code:</p>
                            <div id="google-apis-qr-code"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<?php if (strpos($_SERVER['REQUEST_URI'], "webcams") !== false){ ?>
    <!-- Modal newsitem, only visible when on /webcams -->
    <footer id="webcamLivestream" class="modal fade">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div id="modal-header" class="modal-header" style="height:70px;">
                    <h3 class="modal-webcams-title"></h3>
                    <button class="btn btn-dark modal-close-btn" type="button" data-dismiss="modal">×</button>
                </div>
                <div class="modal-body pt-0 pb-0">
                    <div class="row">
                        <div class="col-9 pt-3" style="margin-bottom:-100px;">
                            <div class="modal-webcams-content pt-3" style=""></div>
                            <div class="youtube-overlay"></div>
                        </div>
                        <div class="col-3 pt-3 bg-light text-center">
                            <h5>Mobiel verder kijken?</h5>
                            <p>Scan de QR-code:</p>
                            <div id="google-apis-qr-code"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
<?php } ?>

<!-- Alert internet connection -->
<footer id="alertInternetConnection" class="modal fade" data-backdrop="static" data-keyboard="false" role="dialog" aria-labelledby="staticBackdropLabel">
    <div class="modal-dialog modal-dialog-centered modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Geen internetverbinding</h4>
            </div>
            <div class="modal-body">
                <p>Het lijkt er op dat je geen werkende internetverbinding hebt.</p>
                <p>Deze melding verdwijnt automatisch wanneer er een werkend internetverbinding beschikbaar is.</p>
            </div>
        </div>
    </div>
</footer>
<footer>
    <div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">Geen internetverbinding!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            </div>
        </div>
    </div>
</footer>


<!-- Default footer -->
<footer class="footer mt-auto py-2 bg-light fixed-bottom">
    <div class="container text-center text-muted small">
        <p class="mb-1">© 2020 - Ondernemend&nbsp;Renesse Aan Zee&nbsp;</p>
    </div>
</footer>
<?php wp_footer();?>
</body>
</html>