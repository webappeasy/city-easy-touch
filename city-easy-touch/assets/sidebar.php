<div class="bg-light col-xl-2 text-right pt-3 shadow" data-aos="fade-left" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__right">
    <h4 class="text-muted">Nieuwsbrief</h4>
    <p class="text-muted">Blijf op de hoogte</p>
    <form method="post" action="<?php echo get_theme_file_uri();?>/mailchimp-subscribe-api.php">
        <div class="form-group">
            <input class="form-control mb-2 use-keyboard-input" type="text" placeholder="Je voornaam" name="fname">
            <input class="form-control mb-2 use-keyboard-input" type="text" placeholder="Je achternaam" name="lname">
            <input class="form-control mb-2 use-keyboard-input" type="email" placeholder="Je e-mailadres" name="email">
            <input class="form-control btn btn-primary mb-2 text-left" type="submit" value="Direct inschrijven" name="submit">
        </div>
    </form>   
    <hr>
    <h4 class="text-muted">Renesse app</h4>
    <p class="text-muted">Dowload in de app-store</p><img class="p-1" src="<?php echo get_theme_file_uri();?>/img/renesse-app-1.png" style="width: 100%;">
</div>
