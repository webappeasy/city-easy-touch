<?php include_once 'assets/header.php'; ?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
    <h4 class="text-muted" id="explore-filters-1">Filters</h4>
    <hr>
    <h5 class="text-muted">Zoekfilter</h5>
    <input class="form-control use-keyboard-input" id="filterInput" type="text" placeholder="Zoeken..">
    <h5 class="text-muted pt-3">Waar</h5>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="binnenRenesse" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="binnenRenesse">Binnen Renesse</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="buitenRenesse" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="buitenRenesse">Buiten Renesse</label></div>
    <h5 class="text-muted pt-3">Categorieën</h5>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkCulinair" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="checkCulinair">Culinair</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkDiensten" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="checkDiensten">Diensten</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkOvernachten" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="checkOvernachten">Overnachten</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkUitgaan" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="checkUitgaan">Uitgaan</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkWinkelen" data-toggle="switchbutton" data-size="sm" data-onlabel="Aan" data-offlabel="Uit"><label class="form-check-label pl-2" for="checkWinkelen">Winkelen</label></div>
    <hr>
    <h6 id="explore-filters-count-items" class="text-muted">... resultaten</h6>
    
    <div class="scrollDiv">
        <button id="topBut" class="btn btn-primary d-block disabled"><i class="fa fa-arrow-up"></i></button>
        <button id="downBut" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div>
        <?php 
            if (have_posts()) : 
                while (have_posts()) : the_post();
                    echo the_content();       
                endwhile; 
            endif;
        ?>
        <div class="row animated fadeIn">
            <div class="container-fluid">
                <div class="loader m-auto"></div>
                <div id="javascript-explore"></div>
                <script src="<?php echo get_template_directory_uri() . '/js/api-explore.js';?>"></script>
            </div>
        </div>
    </div>
</div>

<!-- Right sidebar -->
<?php 
include_once 'assets/sidebar.php';
// Footer
include_once 'assets/footer.php'; 
?>