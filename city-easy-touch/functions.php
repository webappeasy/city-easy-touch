<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Add Theme Supports
if (function_exists('add_theme_support')) {
    // Custom Logo
    add_theme_support('custom-logo');
    // Thumbnails
    add_theme_support('post-thumbnails');
    // Custom Background
    add_theme_support('custom-background');
}

function enqueue_styles() {
    // Theme Styles
    wp_enqueue_style('jquery-ui-style', get_template_directory_uri() . '/inc/jquery-ui/css/jquery-ui.min.css');
    wp_enqueue_style('bootstrap-style', get_template_directory_uri() . '/inc/bootstrap/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-switch-button', 'https://cdn.jsdelivr.net/gh/gitbrent/bootstrap-switch-button@1.1.0/css/bootstrap-switch-button.min.css');
    wp_enqueue_style('city-easy-touch-style', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('city-easy-touch-style-min', get_template_directory_uri() . '/css/style.min.css');
    wp_enqueue_style('fontawesome-style', get_template_directory_uri() . '/inc/fontawesome/css/all.min.css');
    wp_enqueue_style('material-icons', get_template_directory_uri() . '/fonts/material-icons.min.css');
    wp_enqueue_style('font-awesome-5', get_template_directory_uri() . '/fonts/fontawesome5-overrides.min.css');
    wp_enqueue_style('material-icons', 'https://fonts.googleapis.com/icon?family=Material+Icons');
    wp_enqueue_style('keyboard-style', get_template_directory_uri() . '/css/keyboard.css');
    wp_enqueue_style('google-fonts-allerta', 'https://fonts.googleapis.com/css?family=Allerta');
    wp_enqueue_style('google-fonts-allerta', 'https://fonts.googleapis.com/css?family=Roboto');

    // Theme Scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-ui-script', get_template_directory_uri() . '/inc/jquery-ui/js/jquery-ui.min.js', NULL, false, true);
    wp_enqueue_script('jquery-touch-script', get_template_directory_uri() . '/inc/jquery-ui/js/jquery.ui.touch-punch.min.js', NULL, false, true);
    wp_enqueue_script('bootstrap-script', get_template_directory_uri() . '/inc/bootstrap/js/bootstrap.min.js', NULL, false, true);
    wp_enqueue_script('bootstrap-switch-script', get_template_directory_uri() . '/js/bootstrap-switch-button.min.js', NULL, false, true);
    wp_enqueue_script('leaflet-script', get_template_directory_uri() . '/inc/leaflet/js/leaflet.min.js', NULL, false, true);
    wp_enqueue_script('leaflet-gesture-script', get_template_directory_uri() . '/inc/leaflet/js/leaflet-gesture-handling.min.js', NULL, false, true);
    wp_enqueue_script('leaflet-cluster-script', get_template_directory_uri() . '/inc/leaflet/js/leaflet.markercluster.min.js', NULL, false, true);
    wp_enqueue_script('owl-script', get_template_directory_uri() . '/inc/owl-carousel/owl.carousel.min.js', NULL, false, true);
    wp_enqueue_script('keyboard-script', get_template_directory_uri() . '/js/keyboard.js');
    wp_enqueue_script('main-script', get_template_directory_uri() . '/js/main.js');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

$my_current_lang = apply_filters( 'wpml_current_language', NULL );