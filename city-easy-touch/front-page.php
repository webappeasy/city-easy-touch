<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Include header.php
include_once 'assets/header.php';
?>

<div class="col-xl-2"></div>
<div class="col-xl-8 pt-4">
    <div class="row">
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/festival-aan-zee-2019.jpg');">
                <a href="events">
                    <div id="info-card-1" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right"><?php echo $eventText; ?></h5>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/Projecten-6951.jpg');">
                <a href="explore">
                    <div id="info-card-2" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right"><?php echo $discoverText; ?></h5>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/paardrijden-scaled.jpg');">
                <a href="activiteiten">
                    <div id="info-card-3" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right"><?php echo $seedoText; ?></h5>
                </a>
            </div>
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/DJI_0009.JPG');">
                <a href="news">
                    <div id="info-card-4" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right"><?php echo $newsText; ?></h5>
                </a>
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/strandovergang.jpg');">
                <a href="general">
                    <div id="info-card-5" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right"><?php echo $generalText; ?></h5>
                </a>            
            </div>
        </div>
        <div class="col-12 col-lg-4 mb-4 mb-lg-0">
            <div class="col p-0 explore-card bg-light rounded shadow" style="background-image: url('<?php echo get_theme_file_uri();?>/img/banner-webcams.jpg');">
                <a href="webcams">
                    <div id="info-card-6" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                    <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right">Webcams</h5>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- Right sidebar -->
<?php
switch ($my_current_lang) {
    case 'de':
        include_once 'assets/sidebar_de.php';
        include_once 'assets/footer_de.php';
    break;
    case 'en':
        include_once 'assets/sidebar_en.php';
        include_once 'assets/footer_en.php';
    break;
    default:
        include_once 'assets/sidebar.php';
        include_once 'assets/footer.php';
}
?>