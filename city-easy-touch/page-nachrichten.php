<?php include_once 'assets/header.php'; ?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
    <h4 class="text-muted" id="activities-filters-1">Filters</h4>
    <hr>
    <h5 class="text-muted">Suchfilter</h5>
    <input class="form-control use-keyboard-input" id="filterInput" type="text" placeholder="Suchen..">
    <h5 class="text-muted pt-3">Archiv</h5>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="2020" data-toggle="switchbutton" data-size="sm" data-onlabel="Auf" data-offlabel="Von"><label class="form-check-label pl-2" for="2020">2020</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="2019" data-toggle="switchbutton" data-size="sm" data-onlabel="Auf" data-offlabel="Von"><label class="form-check-label pl-2" for="2019">2019</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="2018" data-toggle="switchbutton" data-size="sm" data-onlabel="Auf" data-offlabel="Von"><label class="form-check-label pl-2" for="2018">2018</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="2017" data-toggle="switchbutton" data-size="sm" data-onlabel="Auf" data-offlabel="Von"><label class="form-check-label pl-2" for="2017">2017</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="2016" data-toggle="switchbutton" data-size="sm" data-onlabel="Auf" data-offlabel="Von"><label class="form-check-label pl-2" for="2016">2016</label></div>
    <hr>
    <h6 id="news-filters-count-items" class="text-muted">... Ergebnisse</h6>
    
    <div class="scrollDiv">
        <button id="topBut" class="btn btn-primary d-block disabled"><i class="fa fa-arrow-up"></i></button>
        <button id="downBut" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div>
        <?php 
            if (have_posts()) : 
                while (have_posts()) : the_post();
                    echo the_content();       
                endwhile; 
            endif;
        ?>
        <div class="row animated fadeIn">
            <div class="container-fluid">
                <div class="loader m-auto"></div>
                <div id="javascript-news"></div>
                <script src="<?php echo get_template_directory_uri() . '/js/api-news.js';?>"></script>
            </div>
        </div>
    </div>
</div>

<!-- Right sidebar -->
<?php 
include_once 'assets/sidebar_de.php';
// Footer
include_once 'assets/footer_de.php'; 
?>