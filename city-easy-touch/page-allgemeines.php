<?php include_once 'assets/header.php'; ?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
    <h4 class="text-muted" id="explore-filters-1 text-muted"><?php echo the_title(); ?></h4></a>
    <hr>
    <div>
        <?php 
        $args = array(
            'post_type'      => 'page',
            'posts_per_page' => -1,
            'post_parent'    => $post->ID,
            'order'          => 'ASC',
            'orderby'        => 'menu_order'
         );
        $parent = new WP_Query( $args );
        if ( $parent->have_posts() ) : ?>
            <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>
                <div id="parent-<?php the_ID(); ?>" class="parent-page">
                    <h5><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="text-muted"><?php the_title(); ?></a></h5>
                </div>
            <?php endwhile; ?>
        <?php endif; wp_reset_postdata(); ?>
    </div>
    <div class="scrollDiv">
        <button id="topBut" class="btn btn-primary d-block disabled"><i class="fa fa-arrow-up"></i></button>
        <button id="downBut" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div>
        <div id="algemeen" class="bg-light rounded shadow p-3 mb-5">
            <?php
                if (have_posts()) : 
                    while (have_posts()) : the_post();
                        $content = get_the_content();
                        $content = apply_filters( 'the_content' , $content);
                        $content = strip_tags($content ,'<p><strong><li><ul><b><i><h2><h3>');
                        echo $content;
                    endwhile; 
                endif;
            ?>
        </div>
    </div>
</div>

<!-- Right sidebar -->
<?php 
include_once 'assets/sidebar_de.php';
// Footer
include_once 'assets/footer_de.php'; 
?>