// Load jQuery function
jQuery(document).ready(function($){ 

    $(window).load(function() {
        $(".loader").delay("1500").fadeOut("500");
    });
    
    var language = $(location).attr('pathname');
        language.indexOf(1);
        language.toLowerCase();
        language = language.split("/")[1];

    const app = $('#javascript-events')
         
    // Connect to the Renesse Aan Zee API
    $(document).ready(function () {
        
        if (language === 'de') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/events?lang=de&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {            

            $(data).each(function(index, event) {
                
                var eventEndTime = event.calEnd
                eventEndTime = eventEndTime.slice(-5);

                if (event.time == "Jetzt aktiv") {
                    var eventColor = "#28a745";
                    var eventTime = event.time + " bis " + eventEndTime;
                } else {
                    var eventColor = "#dc3545";
                    var eventTime = event.time;
                }

                if(event.Tags) {
                var eventTags = event.Tags.join(', ');
                }

                // Delete HTML tags of event content
                var eventContentHTML = event.content
                let eventContent = eventContentHTML.replace(/(<([^>]+)>)/gi, "");

                // Event location delete spaces
                var eventLocationSpace = event.Wo
                let eventLocation = eventLocationSpace.replace(" ", "");

                var eventCategory = event.Kategorien

                var html = `
                <div class="row shadow border-0 rounded m-3 single-event-post ${eventLocation} ${eventCategory}">
                    <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                        <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${event.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                            <a href="#evewntInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                                <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right">${event.date}</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                        <a href="#eventInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                            <h2>${event.title}</h2>
                        </a>
                        <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.date}</p>
                        </div>
                        <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2" style="color: ${eventColor};">${eventTime}</p>
                        </div>
                        <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.tickets}</p>
                        </div>
                        <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                            <p class="d-inline-block pl-2">${event.fullLocation}</p>
                        </div>
                        <div>
                            <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Kategorien: </strong>${eventCategory}</p>
                            <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Stichworte: </strong>${eventTags}</p>
                        </div>
                    </div>
                </div>
                `
                $(html).appendTo(app)
                    
                // Fill the modal with content
                $('#eventInformation').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) 
                    var eventTitle = button.data('event-title')
                    // var eventContent = button.data('event-content')
                    var eventQRCode = button.data('event-qr-code')
                    var eventDate = button.data('event-date')
                    var eventThumbnail = button.data('event-thumbnail')
                    var eventPhone = button.data('event-phone')
                    var eventEmail = button.data('event-email')
                    var modal = $(this)
                    modal.find('.modal-event-title').text(eventTitle)
                    modal.find('.modal-event-content').text(eventContent)
                    modal.find('#google-apis-qr-code').html(eventQRCode)
                    modal.find('.modal-event-date').text(eventDate)
                    modal.find('.modal-event-phone').text(eventPhone)
                    modal.find('.modal-event-email').text(eventEmail)
                    modal.find('#modal-header').css({'background-image':'url(' + eventThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                  })

            })
        })
        }

        if (language === 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/events?lang=en&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {            

            $(data).each(function(index, event) {
                
                var eventEndTime = event.calEnd
                eventEndTime = eventEndTime.slice(-5);

                if (event.time == "Now active") {
                    var eventColor = "#28a745";
                    var eventTime = event.time + " from " + eventEndTime;
                } else {
                    var eventColor = "#dc3545";
                    var eventTime = event.time;
                }

                if(event.Tags) {
                var eventTags = event.Tags.join(', ');
                }

                // Delete HTML tags of event content
                var eventContentHTML = event.content
                let eventContent = eventContentHTML.replace(/(<([^>]+)>)/gi, "");

                // Event location delete spaces
                var eventLocationSpace = event.Where
                let eventLocation = eventLocationSpace.replace(" ", "");

                var eventCategory = event.Categories

                var html = `
                <div class="row shadow border-0 rounded m-3 single-event-post ${eventLocation} ${eventCategory}">
                    <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                        <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${event.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                            <a href="#evewntInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                                <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right">${event.date}</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                        <a href="#eventInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                            <h2>${event.title}</h2>
                        </a>
                        <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.date}</p>
                        </div>
                        <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2" style="color: ${eventColor};">${eventTime}</p>
                        </div>
                        <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.tickets}</p>
                        </div>
                        <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                            <p class="d-inline-block pl-2">${event.fullLocation}</p>
                        </div>
                        <div>
                            <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>categories: </strong>${eventCategory}</p>
                            <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>tags: </strong>${eventTags}</p>
                        </div>
                    </div>
                </div>
                `
                $(html).appendTo(app)
                    
                // Fill the modal with content
                $('#eventInformation').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) 
                    var eventTitle = button.data('event-title')
                    // var eventContent = button.data('event-content')
                    var eventQRCode = button.data('event-qr-code')
                    var eventDate = button.data('event-date')
                    var eventThumbnail = button.data('event-thumbnail')
                    var eventPhone = button.data('event-phone')
                    var eventEmail = button.data('event-email')
                    var modal = $(this)
                    modal.find('.modal-event-title').text(eventTitle)
                    modal.find('.modal-event-content').text(eventContent)
                    modal.find('#google-apis-qr-code').html(eventQRCode)
                    modal.find('.modal-event-date').text(eventDate)
                    modal.find('.modal-event-phone').text(eventPhone)
                    modal.find('.modal-event-email').text(eventEmail)
                    modal.find('#modal-header').css({'background-image':'url(' + eventThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                  })

            })
        })
        }

        if (language !== 'de' && language !== 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/events?lang=nl&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {            

            $(data).each(function(index, event) {
                
                var eventEndTime = event.calEnd
                eventEndTime = eventEndTime.slice(-5);

                if (event.time == "Nu actief") {
                    var eventColor = "#28a745";
                    var eventTime = event.time + " tot " + eventEndTime;
                } else {
                    var eventColor = "#dc3545";
                    var eventTime = event.time;
                }

                if(event.Tags) {
                var eventTags = event.Tags.join(', ');
                }

                // Delete HTML tags of event content
                var eventContentHTML = event.content
                let eventContent = eventContentHTML.replace(/(<([^>]+)>)/gi, "");

                // Event location delete spaces
                var eventLocationSpace = event.Waar
                let eventLocation = eventLocationSpace.replace(" ", "");

                var eventCategory = event.Categorieën

                var html = `
                <div class="row shadow border-0 rounded m-3 single-event-post ${eventLocation} ${eventCategory}">
                    <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                        <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${event.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                            <a href="#evewntInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                                <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                <h5 class="d-xl-flex justify-content-xl-center bg-primary shadow text-white position-absolute mt-3 p-3 rounded-right">${event.date}</h5>
                            </a>
                        </div>
                    </div>
                    <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                        <a href="#eventInformation" class="modal-toggle-button" data-toggle="modal" data-target="#eventInformation" data-event-title="${event.title}" data-event-content="${eventContent}" data-event-thumbnail="${event.thumbnail}" data-event-date="${event.date}" data-event-phone="${event.phone}" data-event-email="${event.email}" data-event-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${event.url}&choe=UTF-8">'>
                            <h2>${event.title}</h2>
                        </a>
                        <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.date}</p>
                        </div>
                        <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                            <p class="d-inline-block pl-2" style="color: ${eventColor};">${eventTime}</p>
                        </div>
                        <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                            <p class="d-inline-block pl-2 mb-0">${event.tickets}</p>
                        </div>
                        <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                            <p class="d-inline-block pl-2">${event.fullLocation}</p>
                        </div>
                        <div>
                            <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${eventCategory}</p>
                            <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${eventTags}</p>
                        </div>
                    </div>
                </div>
                `
                $(html).appendTo(app)
                    
                // Fill the modal with content
                $('#eventInformation').on('show.bs.modal', function (event) {
                    var button = $(event.relatedTarget) 
                    var eventTitle = button.data('event-title')
                    // var eventContent = button.data('event-content')
                    var eventQRCode = button.data('event-qr-code')
                    var eventDate = button.data('event-date')
                    var eventThumbnail = button.data('event-thumbnail')
                    var eventPhone = button.data('event-phone')
                    var eventEmail = button.data('event-email')
                    var modal = $(this)
                    modal.find('.modal-event-title').text(eventTitle)
                    modal.find('.modal-event-content').text(eventContent)
                    modal.find('#google-apis-qr-code').html(eventQRCode)
                    modal.find('.modal-event-date').text(eventDate)
                    modal.find('.modal-event-phone').text(eventPhone)
                    modal.find('.modal-event-email').text(eventEmail)
                    modal.find('#modal-header').css({'background-image':'url(' + eventThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                  })

            })
        })
        }
        
    })

    // Search keyboard filter
    $('#filterInput').on('keyup', function() {
        var value = $('#filterInput').val().toLowerCase();
        $('.single-event-post').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
        });
    });

    if ($('#searchVal').length) {
        $('#javascript-events').on('DOMSubtreeModified', function(){
            var value = $('#searchVal').val().toLowerCase();
            $('.single-event-post').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });   
          });
    }

    // Search filter location
    $("#binnenRenesse").change(function() {
        $(".BinnenRenesse").toggle();
        $(".InnenRenesse").toggle();
        $(".InsideRenesse").toggle();
    });

    $("#buitenRenesse").change(function() {
        $(".BuitenRenesse").toggle();
        $(".DraußenRenesse").toggle();
        $(".OutsideRenesse").toggle();
    });

    // Search filter categories
    $("#checkEvenementen").change(function() {
        $(".Evenementen").toggle();
        $(".Ereignisse").toggle();
        $(".Events").toggle();
    });
    $("#checkExcursies").change(function() {
        $(".Excursies").toggle();
        $(".Ausfluge").toggle();
        $(".Field-trips").toggle();
    });
    $("#checkTentoonstellingen").change(function() {
        $(".Tentoonstellingen").toggle();
        $(".Ausstellungen").toggle();
        $(".Exhibitions").toggle();
    });
    
    // Event results counter
    setInterval(function() {
        var eventItems = $("div.single-event-post").length;
        var eventItemsHidden = $("div.single-event-post:hidden").length;
        var eventItemsCount = eventItems - eventItemsHidden;
        //console.log(eventItems)
        if (language === 'de') {
            $("#event-filters-count-items").text(eventItemsCount + " Ergebnisse");
            $('#resultsRightPage').html($('.parent-page').length);
            $('#resultsRightEvent').html(eventItemsCount);
        }
        if (language === 'en') {
            $("#event-filters-count-items").text(eventItemsCount + " results");
            $('#resultsRightPage').html($('.parent-page').length);
            $('#resultsRightEvent').html(eventItemsCount);
        }
        if (language !== 'de' && language !== 'en') {
            $("#event-filters-count-items").text(eventItemsCount + " resultaten");
            $('#resultsRightPage').html($('.parent-page').length);
            $('#resultsRightEvent').html(eventItemsCount);
        }
    }, 300);

    // Automated redirect to homepage after 15 minutes of inactivity
    $(function(){

        idleTime = 0;
     
        //Increment the idle time counter every second.
        var idleInterval = setInterval(timerIncrement, 1000);
     
        function timerIncrement()
        {
          idleTime++;
          if (idleTime > 900)
          {
            doPreload();
          }
        }
     
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function(e){
           idleTime = 0;
        });
     
        function doPreload()
        {
            document.location.href="/";
        }
     
    })

});
