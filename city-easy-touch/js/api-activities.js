// Load jQuery function
jQuery(document).ready(function($){ 

    $(window).load(function() {
        $(".loader").delay("1500").fadeOut("500");
    });

    var language = $(location).attr('pathname');
        language.indexOf(1);
        language.toLowerCase();
        language = language.split("/")[1];

    const app = $('#javascript-activities')
         
    // Connect to the Renesse Aan Zee API
    $(document).ready(function () {
        if (language === 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/activities?lang=en&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, activity) {
                    
                    if (activity.time == "Now open") {
                        var activityColor = "#28a745";
                        var activityTime = activity.time;
                    } else if (activityTime == "Closed") {
                        var activityColor = "#dc3545";
                        var activityTime = activity.time;
                    } else {
                        var activityColor = "#6c757d";
                        var activityTime = activity.time;
                    }

                    if(activity.Tags) {
                    var activityTags = activity.Tags.join(', ');
                    }

                    var activityCategorieën = activity.Categories.join(', ');

                    // Delete HTML tags of activity content
                    var activityContentHTML = activity.content
                    let activityContent = activityContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // activity location delete spaces
                    var activityLocationSpace = activity.Where
                    let activityLocation = activityLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-activity-post ${activityLocation} ${activityCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${activity.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                <h2>${activity.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${activityColor};">${activityTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${activity.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${activityCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${activityTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#activityInformation').on('show.bs.modal', function (activity) {
                        var button = $(activity.relatedTarget) 
                        var activityTitle = button.data('activity-title')
                        var activityContent = button.data('activity-content')
                        var activityQRCode = button.data('activity-qr-code')
                        var activityThumbnail = button.data('activity-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-activity-title').text(activityTitle)
                        modal.find('.modal-activity-content').text(activityContent)
                        modal.find('#google-apis-qr-code').html(activityQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + activityThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }

        if (language === 'de') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/activities?lang=de&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, activity) {
                    
                    if (activity.time == "Jetzt öffnen") {
                        var activityColor = "#28a745";
                        var activityTime = activity.time;
                    } else if (activityTime == "Geschlossen") {
                        var activityColor = "#dc3545";
                        var activityTime = activity.time;
                    } else {
                        var activityColor = "#6c757d";
                        var activityTime = activity.time;
                    }

                    if(activity.Tags) {
                    var activityTags = activity.Tags.join(', ');
                    }

                    var activityCategorieën = activity.Kategorien.join(', ');

                    // Delete HTML tags of activity content
                    var activityContentHTML = activity.content
                    let activityContent = activityContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // activity location delete spaces
                    var activityLocationSpace = activity.Wo
                    let activityLocation = activityLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-activity-post ${activityLocation} ${activityCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${activity.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                <h2>${activity.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${activityColor};">${activityTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${activity.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${activityCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${activityTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#activityInformation').on('show.bs.modal', function (activity) {
                        var button = $(activity.relatedTarget) 
                        var activityTitle = button.data('activity-title')
                        var activityContent = button.data('activity-content')
                        var activityQRCode = button.data('activity-qr-code')
                        var activityThumbnail = button.data('activity-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-activity-title').text(activityTitle)
                        modal.find('.modal-activity-content').text(activityContent)
                        modal.find('#google-apis-qr-code').html(activityQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + activityThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }

        if (language !== 'de' && language !== 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/activities?key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, activity) {
                    
                    if (activity.time == "Nu geopend") {
                        var activityColor = "#28a745";
                        var activityTime = activity.time;
                    } else if (activityTime == "Gesloten") {
                        var activityColor = "#dc3545";
                        var activityTime = activity.time;
                    } else {
                        var activityColor = "#6c757d";
                        var activityTime = activity.time;
                    }

                    if(activity.Tags) {
                    var activityTags = activity.Tags.join(', ');
                    }

                    var activityCategorieën = activity.Categorieën.join(', ');

                    // Delete HTML tags of activity content
                    var activityContentHTML = activity.content
                    let activityContent = activityContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // activity location delete spaces
                    var activityLocationSpace = activity.Waar
                    let activityLocation = activityLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-activity-post ${activityLocation} ${activityCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${activity.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#activityInformation" class="modal-toggle-button" data-toggle="modal" data-target="#activityInformation" data-activity-title="${activity.title}" data-activity-content="${activityContent}" data-activity-thumbnail="${activity.thumbnail}" data-activity-date="${activity.date}" data-activity-phone="${activity.phone}" data-activity-email="${activity.email}" data-activity-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${activity.url}&choe=UTF-8">'>
                                <h2>${activity.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${activityColor};">${activityTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${activity.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${activity.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${activityCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${activityTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#activityInformation').on('show.bs.modal', function (activity) {
                        var button = $(activity.relatedTarget) 
                        var activityTitle = button.data('activity-title')
                        var activityContent = button.data('activity-content')
                        var activityQRCode = button.data('activity-qr-code')
                        var activityThumbnail = button.data('activity-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-activity-title').text(activityTitle)
                        modal.find('.modal-activity-content').text(activityContent)
                        modal.find('#google-apis-qr-code').html(activityQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + activityThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }
    })

    // Search keyboard filter
    $('#filterInput').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.single-activity-post').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    if ($('#searchVal').length) {
        $('#javascript-activities').on('DOMSubtreeModified', function(){
            var value = $('#searchVal').val().toLowerCase();
            $('.single-activity-post').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });   
        });
    }

    // Search filter location
    $("#binnenRenesse").change(function() {
        $(".BinnenRenesse").toggle();
        $(".InsideRenesse").toggle();
        $(".InnenRenesse").toggle();
    });

    $("#buitenRenesse").change(function() {
        $(".BuitenRenesse").toggle();
        $(".OutsideRenesse").toggle();
        $(".DraußenRenesse").toggle();
    });

    // Search filter categories
    $("#checkActiviteiten").change(function() {
        $(".Activiteiten").toggle();
        $(".Activities").toggle();
        $(".Aktivitaten").toggle();
    });
    $("#checkBezichtigen").change(function() {
        $(".Bezichtigen").toggle();
        $(".Viewing").toggle();
        $(".Anzeigen").toggle();
    });
    $("#checkNatuur").change(function() {
        $(".Natuur").toggle();
        $(".Nature").toggle();
        $(".Natur").toggle();
    });
    $("#checkStedenDorpen").change(function() {
        $(".Steden-dorpen").toggle();
        $(".Cities-villages").toggle();
        $(".Stadte-dorfer").toggle();
    });
    
    // activity results counter
    setInterval(function() {
        var activityItems = $("div.single-activity-post").length;
        var activityItemsHidden = $("div.single-activity-post:hidden").length;
        var activityItemsCount = activityItems - activityItemsHidden;
        //console.log(activityItems)
        if (language === 'de') {
            $("#activity-filters-count-items").text(activityItemsCount + " ergebnisse");
            $('#resultsRightZieDoe').html(activityItemsCount);
        }
        if (language === 'en') {
            $("#activity-filters-count-items").text(activityItemsCount + " results");
            $('#resultsRightZieDoe').html(activityItemsCount);
        }
        if (language !== 'de' && language !== 'en') {
            $("#activity-filters-count-items").text(activityItemsCount + " resultaten");
            $('#resultsRightZieDoe').html(activityItemsCount);
        }
    }, 300);

    // Automated redirect to homepage after 15 minutes of inactivity
    $(function(){

        idleTime = 0;
     
        //Increment the idle time counter every second.
        var idleInterval = setInterval(timerIncrement, 1000);
     
        function timerIncrement()
        {
          idleTime++;
          if (idleTime > 900)
          {
            doPreload();
          }
        }
     
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function(e){
           idleTime = 0;
        });
     
        function doPreload()
        {
            document.location.href="/";
        }
     
    })

});
