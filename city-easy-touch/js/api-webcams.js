// Load jQuery function
jQuery(document).ready(function($){ 

    $(window).load(function() {
        $(".loader").delay("750").fadeOut("500");
    });

    var language = $(location).attr('pathname');
        language.indexOf(1);
        language.toLowerCase();
        language = language.split("/")[1];

    const app = $('#javascript-webcams')
         
    // Connect to the Renesse Aan Zee API
    $(document).ready(function () {
        if (language === 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/webcams?lang=en&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, webcams) {
                    
                    var html = `
                    <div class="webcam col-3 mt-4" style="height: 250px;">
                        <a href="#webcamLivestream" class="modal-toggle-button" data-toggle="modal" data-target="#webcamLivestream" data-webcams-title="${webcams.title}" data-webcams-youtube="${webcams.ytID}" data-webcams-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${webcams.url}&choe=UTF-8">'>
                            <div class="webcam-card col h-100 rounded shadow-sm" style="background-image: url(https://img.youtube.com/vi/${webcams.ytID}/0.jpg);background-position:center;">
                                <div id="webcam-icon" class="bg-primary position-absolute shadow rounded-bottom px-3 py-2">
                                    <div class="text-center text-white mt-2">${webcams.title}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    `
                    $(html).appendTo(app)
                                    
                    // Fill the modal with content
                    $('#webcamLivestream').on('show.bs.modal', function (webcams) {
                        var button = $(webcams.relatedTarget) 
                        var webcamsTitle = button.data('webcams-title')
                        var webcamsQRCode = button.data('webcams-qr-code')
                        var webcamsID = button.data('webcams-youtube')
                        var modal = $(this)
                        modal.find('.modal-webcams-title').text(webcamsTitle)
                        modal.find('.modal-webcams-content').html('<iframe class="col " width="560" height="445" src="https://www.youtube.com/embed/' + webcamsID + '?feature=oembed&amp;autoplay=1&amp;modestbranding=1&amp;enablejsapi=1&amp;origin=https://renesseaanzee.nl&amp;disablekb=1&amp;controls=0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0"></iframe>')
                        modal.find('#google-apis-qr-code').html(webcamsQRCode)
                    })
                    
                })
            })
        }

        if (language === 'de') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/webcams?lang=de&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, webcams) {
                    
                    var html = `
                    <div class="webcam col-3 mt-4" style="height: 250px;">
                        <a href="#webcamLivestream" class="modal-toggle-button" data-toggle="modal" data-target="#webcamLivestream" data-webcams-title="${webcams.title}" data-webcams-youtube="${webcams.ytID}" data-webcams-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${webcams.url}&choe=UTF-8">'>
                            <div class="webcam-card col h-100 rounded shadow-sm" style="background-image: url(https://img.youtube.com/vi/${webcams.ytID}/0.jpg);background-position:center;">
                                <div id="webcam-icon" class="bg-primary position-absolute shadow rounded-bottom px-3 py-2">
                                    <div class="text-center text-white mt-2">${webcams.title}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    `
                    $(html).appendTo(app)
                                    
                    // Fill the modal with content
                    $('#webcamLivestream').on('show.bs.modal', function (webcams) {
                        var button = $(webcams.relatedTarget) 
                        var webcamsTitle = button.data('webcams-title')
                        var webcamsQRCode = button.data('webcams-qr-code')
                        var webcamsID = button.data('webcams-youtube')
                        var modal = $(this)
                        modal.find('.modal-webcams-title').text(webcamsTitle)
                        modal.find('.modal-webcams-content').html('<iframe class="col " width="560" height="445" src="https://www.youtube.com/embed/' + webcamsID + '?feature=oembed&amp;autoplay=1&amp;modestbranding=1&amp;enablejsapi=1&amp;origin=https://renesseaanzee.nl&amp;disablekb=1&amp;controls=0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0"></iframe>')
                        modal.find('#google-apis-qr-code').html(webcamsQRCode)
                    })
                    
                })
            })
        }

        if (language !== 'de' && language !== 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/webcams?key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, webcams) {
                    
                    var html = `
                    <div class="webcam col-3 mt-4" style="height: 250px;">
                        <a href="#webcamLivestream" class="modal-toggle-button" data-toggle="modal" data-target="#webcamLivestream" data-webcams-title="${webcams.title}" data-webcams-youtube="${webcams.ytID}" data-webcams-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${webcams.url}&choe=UTF-8">'>
                            <div class="webcam-card col h-100 rounded shadow-sm" style="background-image: url(https://img.youtube.com/vi/${webcams.ytID}/0.jpg);background-position:center;">
                                <div id="webcam-icon" class="bg-primary position-absolute shadow rounded-bottom px-3 py-2">
                                    <div class="text-center text-white mt-2">${webcams.title}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    `
                    $(html).appendTo(app)
                                    
                    // Fill the modal with content
                    $('#webcamLivestream').on('show.bs.modal', function (webcams) {
                        var button = $(webcams.relatedTarget) 
                        var webcamsTitle = button.data('webcams-title')
                        var webcamsQRCode = button.data('webcams-qr-code')
                        var webcamsID = button.data('webcams-youtube')
                        var modal = $(this)
                        modal.find('.modal-webcams-title').text(webcamsTitle)
                        modal.find('.modal-webcams-content').html('<iframe class="col " width="560" height="445" src="https://www.youtube.com/embed/' + webcamsID + '?feature=oembed&amp;autoplay=1&amp;modestbranding=1&amp;enablejsapi=1&amp;origin=https://renesseaanzee.nl&amp;disablekb=1&amp;controls=0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" frameborder="0"></iframe>')
                        modal.find('#google-apis-qr-code').html(webcamsQRCode)
                    })
                    
                })
            })
        }
    })

    // Search keyboard filter
    $('#filterInput').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.single-news-post').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    // Search filter year
    $("#2020").change(function() {
        $(".2020").toggle();
    });
    $("#2019").change(function() {
        $(".2019").toggle();
    });
    $("#2018").change(function() {
        $(".2018").toggle();
    });
    $("#2017").change(function() {
        $(".2017").toggle();
    });
    $("#2016").change(function() {
        $(".2016").toggle();
    });
    
    // news results counter
    setInterval(function() {
        var newsItems = $("div.single-news-post").length;
        var newsItemsHidden = $("div.single-news-post:hidden").length;
        var newsItemsCount = newsItems - newsItemsHidden;
        //console.log(newsItems)
        $("#news-filters-count-items").text(newsItemsCount + " resultaten");
    }, 300);

    // Automated redirect to homepage after 15 minutes of inactivity
    $(function(){

        idleTime = 0;
     
        //Increment the idle time counter every second.
        var idleInterval = setInterval(timerIncrement, 1000);
     
        function timerIncrement()
        {
          idleTime++;
          if (idleTime > 900)
          {
            doPreload();
          }
        }
     
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function(e){
           idleTime = 0;
        });
     
        function doPreload()
        {
            document.location.href="/";
        }
     
    })

});
