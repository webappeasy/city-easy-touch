// Load jQuery function
jQuery(document).ready(function($){ 

    $(window).load(function() {
        $(".loader").delay("1500").fadeOut("500");
    });

    var language = $(location).attr('pathname');
        language.indexOf(1);
        language.toLowerCase();
        language = language.split("/")[1];

    const app = $('#javascript-explore')
         
    // Connect to the Renesse Aan Zee API
    $(document).ready(function () {
        if (language === "en") {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/explore?lang=en&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, explore) {
                    
                    if (explore.time == "Now open") {
                        var exploreColor = "#28a745";
                        var exploreTime = explore.time;
                    } else if (exploreTime == "Closed") {
                        var exploreColor = "#dc3545";
                        var exploreTime = explore.time;
                    } else {
                        var exploreColor = "#6c757d";
                        var exploreTime = explore.time;
                    }

                    if(explore.Tags) {
                    var exploreTags = explore.Tags.join(', ');
                    }

                    var exploreCategorieën = explore.Categories.join(', ');

                    // Delete HTML tags of explore content
                    var exploreContentHTML = explore.content
                    let exploreContent = exploreContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // Explore location delete spaces
                    var exploreLocationSpace = explore.Where
                    let exploreLocation = exploreLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-explore-post ${exploreLocation} ${exploreCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${explore.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                <h2>${explore.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${exploreColor};">${exploreTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${explore.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${exploreCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${exploreTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#exploreInformation').on('show.bs.modal', function (explore) {
                        var button = $(explore.relatedTarget) 
                        var exploreTitle = button.data('explore-title')
                        var exploreContent = button.data('explore-content')
                        var exploreQRCode = button.data('explore-qr-code')
                        var exploreDate = button.data('explore-date')
                        var exploreThumbnail = button.data('explore-thumbnail')
                        var explorePhone = button.data('explore-phone')
                        var exploreEmail = button.data('explore-email')
                        var modal = $(this)
                        modal.find('.modal-explore-title').text(exploreTitle)
                        modal.find('.modal-explore-content').text(exploreContent)
                        modal.find('#google-apis-qr-code').html(exploreQRCode)
                        modal.find('.modal-explore-date').text(exploreDate)
                        modal.find('.modal-explore-phone').text(explorePhone)
                        modal.find('.modal-explore-email').text(exploreEmail)
                        modal.find('#modal-header').css({'background-image':'url(' + exploreThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }

        if (language === "de") {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/explore?lang=de&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, explore) {
                    
                    if (explore.time == "Jetzt öffnen") {
                        var exploreColor = "#28a745";
                        var exploreTime = explore.time;
                    } else if (exploreTime == "Geschlossen") {
                        var exploreColor = "#dc3545";
                        var exploreTime = explore.time;
                    } else {
                        var exploreColor = "#6c757d";
                        var exploreTime = explore.time;
                    }

                    if(explore.Tags) {
                    var exploreTags = explore.Tags.join(', ');
                    }

                    var exploreCategorieën = explore.Kategorien.join(', ');

                    // Delete HTML tags of explore content
                    var exploreContentHTML = explore.content
                    let exploreContent = exploreContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // Explore location delete spaces
                    var exploreLocationSpace = explore.Wo
                    let exploreLocation = exploreLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-explore-post ${exploreLocation} ${exploreCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${explore.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                <h2>${explore.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${exploreColor};">${exploreTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${explore.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${exploreCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${exploreTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#exploreInformation').on('show.bs.modal', function (explore) {
                        var button = $(explore.relatedTarget) 
                        var exploreTitle = button.data('explore-title')
                        var exploreContent = button.data('explore-content')
                        var exploreQRCode = button.data('explore-qr-code')
                        var exploreDate = button.data('explore-date')
                        var exploreThumbnail = button.data('explore-thumbnail')
                        var explorePhone = button.data('explore-phone')
                        var exploreEmail = button.data('explore-email')
                        var modal = $(this)
                        modal.find('.modal-explore-title').text(exploreTitle)
                        modal.find('.modal-explore-content').text(exploreContent)
                        modal.find('#google-apis-qr-code').html(exploreQRCode)
                        modal.find('.modal-explore-date').text(exploreDate)
                        modal.find('.modal-explore-phone').text(explorePhone)
                        modal.find('.modal-explore-email').text(exploreEmail)
                        modal.find('#modal-header').css({'background-image':'url(' + exploreThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});

                        if($(".scrollDiv").length){
                            // Gets scroll buttons
                            var downButton = document.getElementById("downBut");
                            var topButton = document.getElementById("topBut");
                            // Set scroll length
                            var scrollLength = 250;


                            // Adds functions to the scroll buttons
                            downButton.addEventListener("click", () => {scrollDown()});
                            topButton.addEventListener("click", () => {scrollUp()});

                            // Do check always one time
                            checkPageLength();

                            // Do check if objects are added to the main content
                            $('#javascript-explore, #javascript-events, #javascript-activities, #javascript-news').one('DOMSubtreeModified', function(){
                                if ($("#downBut").hasClass("disabled")) {
                                    $("#downBut").removeClass("disabled");
                                }
                            });

                            // Check if scroll buttons are needed
                            function checkPageLength() {
                                if ($(window).height() >= $(document).height()) {
                                    if (!$("#downBut").hasClass("disabled")) {
                                        $("#downBut").addClass("disabled");
                                    }
                            
                                    if (!$("#topBut").hasClass("disabled")) {
                                        $("#topBut").addClass("disabled");
                                    }
                                }
                            }

                            // Scroll down function
                            function scrollDown() {
                                // Check if button is disabled or not
                                if (!$("#downBut").hasClass("disabled")){
                                    if ($(window).scrollTop() + $(window).height() != $(document).height()) {
                                        $(window).scrollTop($(window).scrollTop() + scrollLength);
                                    }
                                }
                            }

                            // Scroll up function
                            function scrollUp() {
                                // Check if button is disabled or not
                                if (!$("#topBut").hasClass("disabled")) {
                                    if ($(window).scrollTop() > 0) {
                                        $(window).scrollTop($(window).scrollTop() - scrollLength);
                                    }
                                }
                            }

                            // Add disabled class or not
                            $(window).scroll(function() {
                                if($(window).scrollTop() + $(window).height() == $(document).height()) {
                                    if (!$("#downBut").hasClass("disabled")) {
                                        $("#downBut").addClass("disabled");
                                    }
                                } else {
                                    if ($("#downBut").hasClass("disabled")) {
                                        $("#downBut").removeClass("disabled");
                                    }
                                }

                                if($(window).scrollTop() <= 0) {
                                    if (!$("#topBut").hasClass("disabled")) {
                                        $("#topBut").addClass("disabled");
                                    }
                                } else {
                                    if ($("#topBut").hasClass("disabled")) {
                                        $("#topBut").removeClass("disabled");
                                    }
                                }
                            });
                        }
                    })
                })
            })
        }

        if (language !== 'de' && language !== 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/explore?lang=nl&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, explore) {
                    
                    if (explore.time == "Nu geopend") {
                        var exploreColor = "#28a745";
                        var exploreTime = explore.time;
                    } else if (exploreTime == "Gesloten") {
                        var exploreColor = "#dc3545";
                        var exploreTime = explore.time;
                    } else {
                        var exploreColor = "#6c757d";
                        var exploreTime = explore.time;
                    }

                    if(explore.Tags) {
                    var exploreTags = explore.Tags.join(', ');
                    }

                    var exploreCategorieën = explore.Categorieën.join(', ');

                    // Delete HTML tags of explore content
                    var exploreContentHTML = explore.content
                    let exploreContent = exploreContentHTML.replace(/(<([^>]+)>)/gi, "");

                    // Explore location delete spaces
                    var exploreLocationSpace = explore.Waar
                    let exploreLocation = exploreLocationSpace.replace(" ", "");


                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-explore-post ${exploreLocation} ${exploreCategorieën}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${explore.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 mb-4 mb-lg-0 pt-3">
                            <a href="#exploreInformation" class="modal-toggle-button" data-toggle="modal" data-target="#exploreInformation" data-explore-title="${explore.title}" data-explore-content="${exploreContent}" data-explore-thumbnail="${explore.thumbnail}" data-explore-date="${explore.date}" data-explore-phone="${explore.phone}" data-explore-email="${explore.email}" data-explore-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${explore.url}&choe=UTF-8">'>
                                <h2>${explore.title}</h2>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.date}</p>
                            </div>
                            <div><i class="fas fa-clock" style="color: rgb(37,41,47);"></i>
                                <p class="d-inline-block pl-2" style="color: ${exploreColor};">${exploreTime}</p>
                            </div>
                            <div><i class="fas fa-ticket-alt fa-fw fa-rotate-135" style="color: #28a745;"></i>
                                <p class="d-inline-block pl-2 mb-0">${explore.tickets}</p>
                            </div>
                            <div><i class="fas fa-map-marker-alt fa-fw fa-rotate-135" style="color: #17a2b8;"></i>
                                <p class="d-inline-block pl-2">${explore.fullLocation}</p>
                            </div>
                            <div>
                                <p class="d-inline-block pl-2 mb-1 text-muted" style="font-size: 14px;"><strong>Categorieën: </strong>${exploreCategorieën}</p>
                                <p class="pl-2 text-muted" style="height: 3rem;font-size: 14px;"><strong>Tags: </strong>${exploreTags}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#exploreInformation').on('show.bs.modal', function (explore) {
                        var button = $(explore.relatedTarget) 
                        var exploreTitle = button.data('explore-title')
                        var exploreContent = button.data('explore-content')
                        var exploreQRCode = button.data('explore-qr-code')
                        var exploreDate = button.data('explore-date')
                        var exploreThumbnail = button.data('explore-thumbnail')
                        var explorePhone = button.data('explore-phone')
                        var exploreEmail = button.data('explore-email')
                        var modal = $(this)
                        modal.find('.modal-explore-title').text(exploreTitle)
                        modal.find('.modal-explore-content').text(exploreContent)
                        modal.find('#google-apis-qr-code').html(exploreQRCode)
                        modal.find('.modal-explore-date').text(exploreDate)
                        modal.find('.modal-explore-phone').text(explorePhone)
                        modal.find('.modal-explore-email').text(exploreEmail)
                        modal.find('#modal-header').css({'background-image':'url(' + exploreThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }
    })

    // Search keyboard filter
    $('#filterInput').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.single-explore-post').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    if ($('#searchVal').length) {
        $('#javascript-explore').on('DOMSubtreeModified', function(){
            var value = $('#searchVal').val().toLowerCase();
            $('.single-explore-post').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });   
        });
    }

    // Search filter location
    $("#binnenRenesse").change(function() {
        $(".BinnenRenesse").toggle();
        $(".InsideRenesse").toggle();
        $(".InnenRenesse").toggle();
    });

    $("#buitenRenesse").change(function() {
        $(".BuitenRenesse").toggle();
        $(".Outside").toggle();
        $(".DraußenRenesse").toggle();
    });

    // Search filter categories
    $("#checkCulinair").change(function() {
        $(".Culinair").toggle();
        $(".Culinary").toggle();
        $(".Kulinarisch").toggle();
    });
    $("#checkDiensten").change(function() {
        $(".Diensten").toggle();
        $(".Services").toggle();
        $(".Dienstleistungen").toggle();
    });
    $("#checkOvernachten").change(function() {
        $(".Overnachten").toggle();
        $(".Stay-overnight").toggle();
        $(".Ubernachtungen").toggle();
    });
    $("#checkUitgaan").change(function() {
        $(".Uitgaan").toggle();
        $(".Go-out").toggle();
        $(".Ausgehen").toggle();
    });
    $("#checkWinkelen").change(function() {
        $(".Winkelen").toggle();
        $(".Shopping").toggle();
        $(".Einkaufen").toggle();
    });
    
    // Explore results counter
    setInterval(function() {
        var exploreItems = $("div.single-explore-post").length;
        var exploreItemsHidden = $("div.single-explore-post:hidden").length;
        var exploreItemsCount = exploreItems - exploreItemsHidden;
        //console.log(exploreItems)
        if (language === 'de') {
            $("#explore-filters-count-items").text(exploreItemsCount + " ergebnisse");
            $('#resultsRightOntdek').html(exploreItemsCount);        }
        if (language === 'en') {
            $("#explore-filters-count-items").text(exploreItemsCount + " results");
            $('#resultsRightOntdek').html(exploreItemsCount);        }
        if (language !== 'de' && language !== 'en') {
            $("#explore-filters-count-items").text(exploreItemsCount + " resultaten");
            $('#resultsRightOntdek').html(exploreItemsCount);
        }
    }, 300);

    // Automated redirect to homepage after 15 minutes of inactivity
    $(function(){

        idleTime = 0;
    
        //Increment the idle time counter every second.
        var idleInterval = setInterval(timerIncrement, 1000);
    
        function timerIncrement()
        {
        idleTime++;
        if (idleTime > 900)
        {
            doPreload();
        }
        }
    
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function(e){
        idleTime = 0;
        });
    
        function doPreload()
        {
            document.location.href="/";
        }
    
    })
});
