jQuery(document).ready(function($){ 

// Warning when no internetconnection
setInterval(function checkconnection() {
    var status = navigator.onLine;
    if (status) {
        $('#alertInternetConnection').hide();
        $('#alertInternetConnection').css('background-color', '');
        $('.container-fluid').css('filter', '');
    } else {
        $('#alertInternetConnection').show();
        $('#alertInternetConnection').css('opacity', '1.0');
        $('#alertInternetConnection').css('background-color', 'rgba(0,0,0,0.3)');
        $('.container-fluid').css('filter', 'grayscale(1)');
        }
}, 300);

$("#searchBut").on("click", () => {
    $("form").submit();
});

if ($(".scrollModel").length) {
    // Get scroll buttons
    var topButton = document.getElementById("topModelButton");
    var downButton = document.getElementById("downModelButton");

    topButton.addEventListener("click", () => {topScroll()});
    downButton.addEventListener("click", () => {downScroll()});

    var scrollLength = 99999;

    $("#topModelButton").addClass("disabled");

    function topScroll() {
        if (!$("#topModelButton").hasClass("disabled")){
            $(".modal").scrollTop($(".modal").scrollTop() - 9999);
            $("#topModelButton").addClass("disabled");
            if ($("#downModelButton").hasClass("disabled")){
                $("#downModelButton").removeClass("disabled");
            }
        }
    }

    function downScroll() {
        if (!$("#downModelButton").hasClass("disabled")){
            $(".modal").scrollTop($(".modal").scrollTop() + 99999);
            $("#downModelButton").addClass("disabled");
            if ($("#topModelButton").hasClass("disabled")){
                $("#topModelButton").removeClass("disabled");
            }
        }
    }

    $('#exploreInformation, #eventInformation, #activityInformation, #newsInformation').on('hidden.bs.modal', function (e) {
        if (!$("#topModelButton").hasClass("disabled")){
            $("#topModelButton").addClass("disabled");
        }
        if ($("#downModelButton").hasClass("disabled")){
            $("#downModelButton").removeClass("disabled");
        }
      })
}

// check if scroll is needed
if($(".scrollDiv").length){
    // Gets scroll buttons
    var downButton = document.getElementById("downBut");
    var topButton = document.getElementById("topBut");
    // Set scroll length
    var scrollLength = 250;


    // Adds functions to the scroll buttons
    downButton.addEventListener("click", () => {scrollDown()});
    topButton.addEventListener("click", () => {scrollUp()});

    // Do check always one time
    checkPageLength();

    // Do check if objects are added to the main content
    $('#javascript-explore, #javascript-events, #javascript-activities, #javascript-news').one('DOMSubtreeModified', function(){
        if ($("#downBut").hasClass("disabled")) {
            $("#downBut").removeClass("disabled");
        }
    });

    // Check if scroll buttons are needed
    function checkPageLength() {
        if ($(window).height() >= $(document).height()) {
            if (!$("#downBut").hasClass("disabled")) {
                $("#downBut").addClass("disabled");
            }
    
            if (!$("#topBut").hasClass("disabled")) {
                $("#topBut").addClass("disabled");
            }
        }
    }

    // Scroll down function
    function scrollDown() {
        // Check if button is disabled or not
        if (!$("#downBut").hasClass("disabled")){
            if ($(window).scrollTop() + $(window).height() != $(document).height()) {
                $(window).scrollTop($(window).scrollTop() + scrollLength);
            }
        }
    }

    // Scroll up function
    function scrollUp() {
        // Check if button is disabled or not
        if (!$("#topBut").hasClass("disabled")) {
            if ($(window).scrollTop() > 0) {
                $(window).scrollTop($(window).scrollTop() - scrollLength);
            }
        }
    }

    // Add disabled class or not
    $(window).scroll(function() {
        if($(window).scrollTop() + $(window).height() == $(document).height()) {
            if (!$("#downBut").hasClass("disabled")) {
                $("#downBut").addClass("disabled");
            }
        } else {
            if ($("#downBut").hasClass("disabled")) {
                $("#downBut").removeClass("disabled");
            }
        }

        if($(window).scrollTop() <= 0) {
            if (!$("#topBut").hasClass("disabled")) {
                $("#topBut").addClass("disabled");
            }
        } else {
            if ($("#topBut").hasClass("disabled")) {
                $("#topBut").removeClass("disabled");
            }
        }
    });
}

// Search page scroll

if ($("#SearchPageResults").length) {
    $("#SearchPageResults").click(function() {
        $('html, body').animate({
            scrollTop: $("#pageResults").offset().top - 375
        }, 500);
    });
}

if ($("#SearchEventResults").length) {
    $("#SearchEventResults").click(function() {
        $('html, body').animate({
            scrollTop: $("#eventResults").offset().top - 375
        }, 500);
    });
}

if ($("#SearchOntdekResults").length) {
    $("#SearchOntdekResults").click(function() {
        $('html, body').animate({
            scrollTop: $("#ontdekResults").offset().top - 375
        }, 500);
    });
}

if ($("#SearchZieDoeResults").length) {
    $("#SearchZieDoeResults").click(function() {
        $('html, body').animate({
            scrollTop: $("#ZieDoResults").offset().top - 375
        }, 500);
    });
}

if ($("#SearchNieuwsResults").length) {
    $("#SearchNieuwsResults").click(function() {
        $('html, body').animate({
            scrollTop: $("#nieuwResults").offset().top - 375
        }, 500);
    });
}

})