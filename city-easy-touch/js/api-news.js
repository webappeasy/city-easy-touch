// Load jQuery function
jQuery(document).ready(function($){ 

    $(window).load(function() {
        $(".loader").delay("750").fadeOut("500");
    });

    var language = $(location).attr('pathname');
        language.indexOf(1);
        language.toLowerCase();
        language = language.split("/")[1];

    const app = $('#javascript-news')
         
    // Connect to the Renesse Aan Zee API
    $(document).ready(function () {

        if (language === 'en') {
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/news?lang=en&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, news) {
                    
                    // Delete HTML tags of news content
                    var newsContentHTML = news.content
                    let newsContent = newsContentHTML.replace(/(<([^>]+)>)/gi, "");

                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-news-post ${news.date}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${news.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8 mb-4 mb-lg-0 pt-3">
                            <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                <h3 class="card-title text-decoration-none">${news.title}</h3>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgba(0,0,0,0.5);"></i>
                                <p class="d-inline-block pl-2 mt-3 mb-0 text-black-50">${news.date}</p>
                            </div>
                            <div>
                                <p class="card-excerpt pb-3">${newsContent}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#newsInformation').on('show.bs.modal', function (news) {
                        var button = $(news.relatedTarget) 
                        var newsTitle = button.data('news-title')
                        var newsContent = button.data('news-content')
                        var newsDate = button.data('news-date')
                        var newsQRCode = button.data('news-qr-code')
                        var newsThumbnail = button.data('news-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-news-title').text(newsTitle)
                        modal.find('.modal-news-content').text(newsContent)
                        modal.find('.modal-news-date').text(newsDate)
                        modal.find('#google-apis-qr-code').html(newsQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + newsThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }

        if (language === 'de'){
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/news?lang=de&key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, news) {
                    
                    // Delete HTML tags of news content
                    var newsContentHTML = news.content
                    let newsContent = newsContentHTML.replace(/(<([^>]+)>)/gi, "");

                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-news-post ${news.date}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${news.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8 mb-4 mb-lg-0 pt-3">
                            <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                <h3 class="card-title text-decoration-none">${news.title}</h3>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgba(0,0,0,0.5);"></i>
                                <p class="d-inline-block pl-2 mt-3 mb-0 text-black-50">${news.date}</p>
                            </div>
                            <div>
                                <p class="card-excerpt pb-3">${newsContent}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#newsInformation').on('show.bs.modal', function (news) {
                        var button = $(news.relatedTarget) 
                        var newsTitle = button.data('news-title')
                        var newsContent = button.data('news-content')
                        var newsDate = button.data('news-date')
                        var newsQRCode = button.data('news-qr-code')
                        var newsThumbnail = button.data('news-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-news-title').text(newsTitle)
                        modal.find('.modal-news-content').text(newsContent)
                        modal.find('.modal-news-date').text(newsDate)
                        modal.find('#google-apis-qr-code').html(newsQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + newsThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }

        if (language !== 'de' && language !== 'en'){
            $.getJSON('https://renesseaanzee.nl/wp-json/app/v1/news?key=83b6ea44-9765-46b9-b391-8fd55ba49015', function(data) {

                $(data).each(function(index, news) {
                    
                    // Delete HTML tags of news content
                    var newsContentHTML = news.content
                    let newsContent = newsContentHTML.replace(/(<([^>]+)>)/gi, "");

                    var html = `
                    <div class="row shadow border-0 rounded m-3 single-news-post ${news.date}">
                        <div class="col-12 col-lg-4 mb-4 mb-lg-0 pl-0">
                            <div class="col p-0 explore-card bg-light shadow-sm h-100" style="background-image: url(${news.thumbnail});border-top-left-radius: .25rem;border-bottom-left-radius: .25rem;">
                                <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                    <div id="info-card" class="col rounded h-100 position-absolute d-flex align-items-center justify-content-center"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-12 col-lg-8 mb-4 mb-lg-0 pt-3">
                            <a href="#newsInformation" class="modal-toggle-button" data-toggle="modal" data-target="#newsInformation" data-news-title="${news.title}" data-news-content="${newsContent}" data-news-thumbnail="${news.thumbnail}" data-news-date="${news.date}" data-news-qr-code='<img src="https://chart.googleapis.com/chart?chs=250x250&cht=qr&chl=${news.url}&choe=UTF-8">'>
                                <h3 class="card-title text-decoration-none">${news.title}</h3>
                            </a>
                            <div><i class="fas fa-calendar-alt" style="color: rgba(0,0,0,0.5);"></i>
                                <p class="d-inline-block pl-2 mt-3 mb-0 text-black-50">${news.date}</p>
                            </div>
                            <div>
                                <p class="card-excerpt pb-3">${newsContent}</p>
                            </div>
                        </div>
                    </div>
                    `
                    $(html).appendTo(app)
                        
                    // Fill the modal with content
                    $('#newsInformation').on('show.bs.modal', function (news) {
                        var button = $(news.relatedTarget) 
                        var newsTitle = button.data('news-title')
                        var newsContent = button.data('news-content')
                        var newsDate = button.data('news-date')
                        var newsQRCode = button.data('news-qr-code')
                        var newsThumbnail = button.data('news-thumbnail')
                        var modal = $(this)
                        modal.find('.modal-news-title').text(newsTitle)
                        modal.find('.modal-news-content').text(newsContent)
                        modal.find('.modal-news-date').text(newsDate)
                        modal.find('#google-apis-qr-code').html(newsQRCode)
                        modal.find('#modal-header').css({'background-image':'url(' + newsThumbnail + ')','background-repeat':'no-repeat','background-size':'cover','background-position':'center'});
                    })
                    
                })
            })
        }
    })

    // Search keyboard filter
    $('#filterInput').on('keyup', function() {
        var value = $(this).val().toLowerCase();
        $('.single-news-post').filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    if ($('#searchVal').length) {
        $('#javascript-news').on('DOMSubtreeModified', function(){
            var value = $('#searchVal').val().toLowerCase();
            $('.single-news-post').filter(function() {
                $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
            });   
        });
    }

    // Search filter year
    $("#2020").change(function() {
        $(".2020").toggle();
    });
    $("#2019").change(function() {
        $(".2019").toggle();
    });
    $("#2018").change(function() {
        $(".2018").toggle();
    });
    $("#2017").change(function() {
        $(".2017").toggle();
    });
    $("#2016").change(function() {
        $(".2016").toggle();
    });
    
    // news results counter
    setInterval(function() {
        var newsItems = $("div.single-news-post").length;
        var newsItemsHidden = $("div.single-news-post:hidden").length;
        var newsItemsCount = newsItems - newsItemsHidden;
        //console.log(newsItems)
        if (language === 'de') {
            $("#news-filters-count-items").text(newsItemsCount + " ergebnisse");
            $('#resultsRightNieuws').html(newsItemsCount);
        }
        if (language === 'en') {
            $("#news-filters-count-items").text(newsItemsCount + " results");
            $('#resultsRightNieuws').html(newsItemsCount);
        }
        if (language !== 'de' && language !== 'en') {
            $("#news-filters-count-items").text(newsItemsCount + " resultaten");
            $('#resultsRightNieuws').html(newsItemsCount);
        }
    }, 300);

    // Automated redirect to homepage after 15 minutes of inactivity
    $(function(){

        idleTime = 0;
     
        //Increment the idle time counter every second.
        var idleInterval = setInterval(timerIncrement, 1000);
     
        function timerIncrement()
        {
          idleTime++;
          if (idleTime > 900)
          {
            doPreload();
          }
        }
     
        //Zero the idle timer on mouse movement.
        $(this).mousemove(function(e){
           idleTime = 0;
        });
     
        function doPreload()
        {
            document.location.href="/";
        }
     
    })

});
