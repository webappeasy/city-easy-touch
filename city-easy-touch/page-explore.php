<?php include_once 'assets/header.php'; ?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
    <h4 class="text-muted" id="explore-filters-1">Filters</h4>
    <hr>
    <h5 class="text-muted">Searchfilter</h5>
    <input class="form-control use-keyboard-input" id="filterInput" type="text" placeholder="Search..">
    <h5 class="text-muted pt-3">Where</h5>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="binnenRenesse" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="binnenRenesse">Inside Renesse</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="buitenRenesse" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="buitenRenesse">Outside Renesse</label></div>
    <h5 class="text-muted pt-3">Categories</h5>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkCulinair" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="checkCulinair">Culinary</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkDiensten" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="checkDiensten">Services</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkOvernachten" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="checkOvernachten">Overnight</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkUitgaan" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="checkUitgaan">Going out</label></div>
    <div class="form-check pl-0 mb-1"><input checked class="form-check-input" type="checkbox" id="checkWinkelen" data-toggle="switchbutton" data-size="sm" data-onlabel="On" data-offlabel="Off"><label class="form-check-label pl-2" for="checkWinkelen">Shopping</label></div>
    <hr>
    <h6 id="explore-filters-count-items" class="text-muted">... results</h6>
    
    <div class="scrollDiv">
        <button id="topBut" class="btn btn-primary d-block disabled"><i class="fa fa-arrow-up"></i></button>
        <button id="downBut" class="btn btn-primary d-block"><i class="fa fa-arrow-down"></i></button>
    </div>
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div>
        <?php 
            if (have_posts()) : 
                while (have_posts()) : the_post();
                    echo the_content();       
                endwhile; 
            endif;
        ?>
        <div class="row animated fadeIn">
            <div class="container-fluid">
                <div class="loader m-auto"></div>
                <div id="javascript-explore"></div>
                <script src="<?php echo get_template_directory_uri() . '/js/api-explore.js';?>"></script>
            </div>
        </div>
    </div>
</div>

<!-- Right sidebar -->
<?php 
include_once 'assets/sidebar_en.php';
// Footer
include_once 'assets/footer_en.php'; 
?>