<?php
// If file is called directly, abort
if (!defined('ABSPATH')) exit;

// Include header.php
include_once 'assets/header.php';
?>

<!-- Left sidebar -->
<div data-aos="fade-right" data-aos-duration="300" data-aos-delay="300" id="sidebar__landscape__left" class="bg-light col-xl-2 pt-3 shadow">
  
</div>

<!-- Main content -->
<div id="main-content" class="col-xl-8 pt-3">
    <div class="row animated fadeIn">
        <div class="container-fluid">
            <div class="m-auto text-center"></div>
                <h2>Oeps, deze link werkt niet!</h2>
                <div>
                    <a href="<?php echo home_url();?>" class="btn btn-primary"><?php _e('Terug naar de startpagina', 'city-easy');?></a>
                </div>
            </div>
    </div>
</div>

<?php
include_once 'assets/sidebar.php';
include_once 'assets/footer.php';